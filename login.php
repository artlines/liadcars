<?php include 'header.php'; ?>
<?php
$error = 0;
if(isset($_POST['user']) && isset($_POST['pass'])){
	$okay = false;

	$user = $_POST['user'];
	$pass = $_POST['pass'];

	$r = $conn->prepare("SELECT * FROM users WHERE username=?");
	$r->bind_param("s", $user);
	$r->execute();
	$result = $r->get_result();
	if($row =  $result->fetch_array(MYSQLI_ASSOC)){
		$okay = true; // Username exists
	} else { $error= 1; } 

	if($okay)
	{
		$r = $conn->prepare("SELECT * FROM users WHERE username=? AND password=?");
		$r->bind_param("ss", $user, $pass);
		$r->execute();
		$result = $r->get_result();
		if($row =  $result->fetch_array(MYSQLI_ASSOC)){
			$_SESSION['user'] = $row['name'];
			if(intval($row['admin']) == 1)
			{
				$_SESSION['admin'] = 1;
			}
			if(intval($row['superuser']) == 1)
			{
				$_SESSION['superuser'] = 1;
			}
			header('Location: index.php');
		} else { $error= 2; } 
	}
}
?>
<!DOCTYPE html>
<html>
<form method="post" action="login.php">
	<span class="error">	 
  		<?php if($error == 1){ echo "שם משתמש לא נכון";}
  				else if($error == 2){echo "סיסמה לא נכונה";};?>
  	</span>
	<input type="text" name="user" value="<?php if(isset($_POST['user'])) echo $_POST['user']; ?>">
	<input type="password" name="pass" placeholder="סיסמה">
	<input type="submit" name="login" class="login loginmodal-submit" value="Login">
</form>
<?php include 'footer.php'; ?>
</body>
</html>